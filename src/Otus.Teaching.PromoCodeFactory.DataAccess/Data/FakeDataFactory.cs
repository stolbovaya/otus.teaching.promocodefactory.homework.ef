﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee(1,1,"Иван","Сергеев","owner@somemail.ru",0),
            
            new Employee(2,2,"Петр","Андреев","andreev@somemail.ru",1)
            
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id =1,
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = 2,
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
 /*           new Preference()
            {
                Id = 1,
                Name = "Театр",
            },
            new Preference()
            {
                Id = 2,
                Name = "Семья",
            },
            new Preference()
            {
                Id = 3,
                Name = "Музыка",
            }
*/
        };

        public static IEnumerable<Customer> Customers => new List<Customer>()
        {
            // Preferences.FirstOrDefault(i => i.Id == 1).ToString;


                new Customer ()
                {Id=1,FirstName="Петр",LastName="Петров",Email="pet@mail",
                 Preferences = {
                            new Preference()
                              {
                               Id = 2,
                               Name = "Семья",
                               },
                    new Preference()
                              {
                               Id = 4,
                               Name = "Кино",
                               }} },
                new Customer()
                {Id=3,FirstName="Сергей",LastName="Сергеев",Email="ser@mail",
                 Preferences = {
                            new Preference()
                              {
                               Id = 3,
                               Name = "Музыка",
                               }} },
                new Customer()
                {Id=2,FirstName="Виталий",LastName="Борисв",Email="vit@mail",
                 Preferences = {
                            new Preference()
                              {
                               Id = 1,
                               Name = "Театр",
                               }}},
                new Customer(){Id=5,FirstName="Георгий",LastName="Семаков",Email="geor@mail"}


         };
        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        {

            new PromoCode()
            {
                Id = 1,
                Code = "1111",
                ServiceInfo = "11111",
                PreferenceId = 2,
                BeginDate = "2003-09-01",
                EndDate = "2003-09-01",
                PartnerManagerId  = 1,
                CustomerId =1
            },
            new PromoCode()
            {
                Id = 2,
                Code = "2222",
                ServiceInfo = "2222",
                PreferenceId =4,
                BeginDate = "2003-09-01",
                EndDate = "2003-09-01",
                PartnerManagerId  = 2,
                CustomerId =1
            }

        };
     /*  public static IEnumerable<CustomerPreference> CustomerPreferences => new List<CustomerPreference>()
        {
            new CustomerPreference()
            {
                CustomerId = 1,
                PreferenceId = 1,
            },
            new CustomerPreference()
            {
                CustomerId = 1,
                PreferenceId = 2,
            },
            new CustomerPreference()
            {
                CustomerId = 2,
                PreferenceId = 2,
            },
             new CustomerPreference()
            {
                CustomerId = 3,
                PreferenceId = 3,
            }

        };*/


    }
}