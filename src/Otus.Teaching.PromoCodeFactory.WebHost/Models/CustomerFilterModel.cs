namespace Otus.Teaching.PromoCodeFactory.WebApi.Models
{ 
public class CustomerFilterModel
{
    public string Email { get; set; }
    
    
    public int ItemsPerPage { get; set; }
    
    public int Page { get; set; }
}
}