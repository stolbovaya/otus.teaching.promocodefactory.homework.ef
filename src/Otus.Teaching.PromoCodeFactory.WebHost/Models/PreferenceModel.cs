﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;


namespace Otus.Teaching.PromoCodeFactory.WebApi.Models
{
    /// <summary>
    /// ДТО курса
    /// </summary>
    public class PreferenceModel
    {
        public int Id { get; set; }
        public string Name { get; set; }



    }
}