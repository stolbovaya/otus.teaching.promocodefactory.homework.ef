﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;


namespace Otus.Teaching.PromoCodeFactory.WebApi.Models
{
    /// <summary>
    /// ДТО курса
    /// </summary>
    public class CustomerShortModel
    {
     //   public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }


    }
}