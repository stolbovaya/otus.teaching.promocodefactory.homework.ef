﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleShortModel
    {

        public string Name { get; set; }

        public string Description { get; set; }

    }
}