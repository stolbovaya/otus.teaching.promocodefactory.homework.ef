﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual List<EmployeeDto> Employee { get; set; }
    }
}