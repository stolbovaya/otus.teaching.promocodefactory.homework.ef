﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;


namespace Otus.Teaching.PromoCodeFactory.WebApi.Models
{
    /// <summary>
    /// ДТО курса
    /// </summary>
    public class CustomerModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes 
        public virtual List<PreferenceDto> Preferences { get; set; }

        public virtual List<PromoCodeDto> PromoCodes { get; set; }
    }
}