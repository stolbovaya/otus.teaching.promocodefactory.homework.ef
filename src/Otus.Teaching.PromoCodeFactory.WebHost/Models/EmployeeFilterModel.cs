﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeFilterModel
    {
        public string Email { get; set; }

        public int ItemsPerPage { get; set; }

        public int Page { get; set; }
    }
}