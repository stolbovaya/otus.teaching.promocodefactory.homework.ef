﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;


namespace Otus.Teaching.PromoCodeFactory.WebApi.Models
{
    /// <summary>
    /// ДТО курса
    /// </summary>
    public class PreferenceFilterModel
    {
        public string Name { get; set; }
        public int ItemsPerPage { get; set; }

        public int Page { get; set; }



    }
}