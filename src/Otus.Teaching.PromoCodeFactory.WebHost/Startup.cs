using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.WebApi.Mapping;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.IO;
using System.Reflection;
using System;
using System.Text.Json.Serialization;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            InstallAutomapper(services);
            services.AddServices(Configuration);
            services.AddControllers()
                .AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
            }); ;
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen((o => AddSwaggerDocumentation(o)));

            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            //app.UseHealthChecks("/health");

            app.UseRouting();

        //    app.UseAuthorization();

         //   app.UseOpenApi();


            if (!env.IsProduction())
            {

                    app.UseSwagger(c =>
                    {
                        c.RouteTemplate = "/swagger/{documentName}/swagger.json";
                    });
                    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "API v1"));
               



            }

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static IServiceCollection InstallAutomapper(IServiceCollection services)
        {
            services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
            return services;
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CustomerMappingsProfile>();
                cfg.AddProfile<PreferenceMappingsProfile>();
                cfg.AddProfile<PromoCodeMappingsProfile>();
                cfg.AddProfile<EmployeeMappingsProfile>();
                cfg.AddProfile<RoleMappingsProfile>();
                cfg.AddProfile<Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping.CustomerMappingsProfile>();
                cfg.AddProfile<Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping.PreferenceMappingsProfile>();
                cfg.AddProfile<Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping.PromoCodeMappingsProfile>();
                cfg.AddProfile<Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping.EmployeeMappingsProfile>();
                cfg.AddProfile<Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping.RoleMappingsProfile>();

            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }

        private static void AddSwaggerDocumentation(SwaggerGenOptions o)
        {
            var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            o.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
        }
    }

}