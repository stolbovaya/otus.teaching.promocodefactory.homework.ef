﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebApi.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private IPreferenceService _service;
        private IMapper _mapper;
        private readonly ILogger<PreferenceController> _logger;

        public PreferenceController(IPreferenceService service, ILogger<PreferenceController> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }
        /// <summary>
        /// Получение предпочтения по id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> ДТО предпочтения</returns>

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(_mapper.Map<PreferenceModel>(await _service.GetById(id)));
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="page,itemsPerPage"> номер страницы и количество элементов на странице. </param>
        /// <returns> Список ДТО предпочтений </returns>
        [HttpGet("list")]
        public async Task<IActionResult> GetList(int page, int itemsPerPage)
        {
            return Ok(_mapper.Map<List<PreferencesResponse>>(await _service.GetPaged(page, itemsPerPage)));
        }
        /// <summary>
        /// Добавление предпочтения.
        /// </summary>
        /// <param name="Предпочтение"></param>
        /// <returns> Id созданного предпочтения</returns>

        [HttpPost]
        public async Task<IActionResult> Add(PreferenceModel preferenceDto)
        {
            return Ok(await _service.Create(_mapper.Map<PreferenceDto>(preferenceDto)));
        }

        /// <summary>
        /// Редактирование названия предпочтения по id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>статус</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(int id, PreferenceModel lessonDto)
        {
            await _service.Update(id, _mapper.Map<PreferenceDto>(lessonDto));
            return Ok();
        }
        /// <summary>
        /// Удаление предпочтения по id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>статус</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return Ok();
        }


    }
}