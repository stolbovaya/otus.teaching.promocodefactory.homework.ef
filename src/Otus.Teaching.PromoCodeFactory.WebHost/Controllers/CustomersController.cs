﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebApi.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]

    public class CustomerController : ControllerBase
    {
        private ICustomerService _service;
        private IMapper _mapper;
        private readonly ILogger<CustomerController> _logger;

        public CustomerController(ICustomerService service, ILogger<CustomerController> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }
        /// <summary>
        /// Получение клиента по id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> ДТО клиента</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(_mapper.Map<CustomerDto>(await _service.GetById(id)));
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="page,itemsPerPage"> номер страницы и количество элементов на странице. </param>
        /// <returns> Список ДТО клиентов </returns>

        [HttpGet("list/{page}/{itemsPerPage}")]
        public async Task<IActionResult> GetList(int page, int itemsPerPage)
        {
            return Ok(_mapper.Map<List<CustomerResponse>>(await _service.GetPaged(page, itemsPerPage)));
        }

        /// <summary>
        /// Добавление клиента.
        /// </summary>
        /// <param name="Краткое ДТО клиента"></param>
        /// <returns> Id клиента</returns>
        [HttpPost]
        public async Task<IActionResult> Add(CustomerShortModel customerRequest)
        {
            return Ok(await _service.Create(_mapper.Map<CustomerDto>(customerRequest)));
        }
        /// <summary>
        /// Добавление клиенту по его id предпочтений по preferenceId.
        /// </summary>
        /// <param name="id,preferenceId"></param>
        /// <returns>ДТО клиента с его предпочтениями</returns>

        [HttpPut("AddPreference/{id}/{preferenceId}")]
        public async Task<IActionResult> Edit(int id, int preferenceId)
        {
            return Ok(_mapper.Map<CustomerResponse>(await _service.AddPreference(id, preferenceId)));
        }

        /// <summary>
        /// Редактирование данных клиента по его id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>статус</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(int id, CustomerShortModel customerDto)
        {
            await _service.Update(id, _mapper.Map<CustomerDto>(customerDto));
            return Ok();
        }


        /// <summary>
        /// Удаление клиента с его промокодами по id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>статус</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return Ok();
        }

    }
}