﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebApi.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeeController : ControllerBase
    {
        private IEmployeeService _service;
        private IMapper _mapper;
        private readonly ILogger<EmployeeController> _logger;

        public EmployeeController(IEmployeeService service, ILogger<EmployeeController> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(_mapper.Map<EmployeeShortModel>(await _service.GetById(id)));
        }

        //[HttpGet("list/{page}/{itemsPerPage}")]
        //public async Task<IActionResult> GetList(int page, int itemsPerPage)
        //{
        //    return Ok(_mapper.Map<List<EmployeeModel>>(await _service.GetPaged(page, itemsPerPage)));
        //}
        [HttpPost]
        public async Task<IActionResult> Add(EmployeeModel customerRequest)
        {
            return Ok(await _service.Create(_mapper.Map<EmployeeDto>(customerRequest)));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(int id, EmployeeModel customerDto)
        {
            await _service.Update(id, _mapper.Map<EmployeeDto>(customerDto));
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return Ok();
        }

    }
}