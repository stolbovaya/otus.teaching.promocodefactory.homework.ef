﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebApi.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController : ControllerBase
    {
        private IPromoCodeService _service;
        private IMapper _mapper;
        private readonly ILogger<PromoCodesController> _logger;

        public PromoCodesController(IPromoCodeService service, ILogger<PromoCodesController> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(_mapper.Map<PromoCodeModel>(await _service.GetById(id)));
        }
        [HttpGet("list")]
        public async Task<IActionResult> GetList(int page, int itemsPerPage)
        {
            return Ok(_mapper.Map<List<PromoCodeModel>>(await _service.GetPaged(page, itemsPerPage)));
        }


        [HttpPost]
        public async Task<IActionResult> Add(PromoCodeShortModel promoCodeShortModel)

        {

            return Ok(await _service.Create(_mapper.Map<PromoCodeDto>(promoCodeShortModel)));
        }

        /// <summary>
        ///  Метод сохраняет новый промокод в базе данных и находить клиентов с переданным предпочтением и добавлять им данный промокод
        /// </summary>
        /// <param name="ДТО промокода"></param>
        /// <returns>id созданного промокода</returns>

        [HttpPost ("GivePromocodesToCustomersWithPreference")]
        public async Task<IActionResult> GivePromocodes(PromoCodeRequest promoCodeShortModel)

        {

            return Ok(await _service.GivePromocodesToCustomersWithPreferenceAsync(_mapper.Map<PromoCodeDto>(promoCodeShortModel)));
        }
        

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return Ok();
        }


    }
}