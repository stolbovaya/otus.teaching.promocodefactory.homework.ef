using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebApi.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности курса.
    /// </summary>
    public class CustomerMappingsProfile : Profile
    {
        public CustomerMappingsProfile()
        {
            CreateMap<CustomerDto, CustomerModel>();
            CreateMap<CustomerModel, CustomerDto>();


            CreateMap<CustomerShortModel,CustomerDto>()
                .ForMember(d => d.Id, map => map.Ignore() )
                .ForMember(d => d.PromoCodes, map => map.Ignore())
                .ForMember(d => d.Preferences, map => map.Ignore()); 

            CreateMap<CustomerFilterModel, CustomerFilterDto>();

            CreateMap<CustomerResponse, CustomerResponseDto>();
            CreateMap<CustomerResponseDto, CustomerResponse>();

           
        }
    }
}
