using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;


namespace Otus.Teaching.PromoCodeFactory.WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности курса.
    /// </summary>
    public class EmployeeMappingsProfile : Profile
    {
        public EmployeeMappingsProfile()
        {
            CreateMap<EmployeeDto, EmployeeModel>();
            CreateMap<EmployeeModel, EmployeeDto>();

            CreateMap<EmployeeShortDto, EmployeeShortModel>();
            
            CreateMap<EmployeeFilterModel, EmployeeFilterDto>();
        }
    }
}
