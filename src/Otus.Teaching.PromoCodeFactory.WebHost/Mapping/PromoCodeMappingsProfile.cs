using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;


namespace Otus.Teaching.PromoCodeFactory.WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности курса.
    /// </summary>
    public class PromoCodeMappingsProfile : Profile
    {
        public PromoCodeMappingsProfile()
        {
            CreateMap<PromoCodeDto, PromoCodeModel>();
            CreateMap<PromoCodeModel, PromoCodeDto>();

            CreateMap<PromoCodeShortModel, PromoCodeDto>()
                .ForMember(d => d.PartnerManager, map => map.Ignore())
                .ForMember(d => d.Preference, map => map.Ignore())
                .ForMember(d => d.Customer, map => map.Ignore());

            CreateMap<PromoCodeShortDto, PromoCodeShortModel>();
            CreateMap<PromoCodeShortModel, PromoCodeShortDto>();

            CreateMap<PromoCodeFilterModel, PromoCodeFilterDto>();


            CreateMap<PromoCodeRequest, PromoCodeDto>()
               .ForMember(d => d.PartnerManager, map => map.Ignore())
               .ForMember(d => d.Preference, map => map.Ignore())
               .ForMember(d => d.Customer, map => map.Ignore())
               .ForMember(d => d.CustomerId, map => map.Ignore())
               .ForMember(d => d.Id, map => map.Ignore())
               ;

        }
    }
}
