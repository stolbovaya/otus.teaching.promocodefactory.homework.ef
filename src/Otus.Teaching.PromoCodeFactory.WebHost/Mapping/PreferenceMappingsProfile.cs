using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebApi.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности курса.
    /// </summary>
    public class PreferenceMappingsProfile : Profile
    {
        public PreferenceMappingsProfile()
        {
            CreateMap<PreferenceDto, PreferenceModel>();
            CreateMap<PreferenceModel, PreferenceDto>();
            
            CreateMap<PreferencesResponse, PreferencesResponseDto>();
            CreateMap<PreferencesResponseDto, PreferencesResponse>();
        }
    }
}
