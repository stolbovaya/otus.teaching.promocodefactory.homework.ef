﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;


namespace Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с покупателями
    /// </summary>
    public interface IEmployeeRepository : IRepository<Employee, int>
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
        Task<List<Employee>> GetPagedAsync(EmployeeFilterDto filterDto);
   
    }
}
