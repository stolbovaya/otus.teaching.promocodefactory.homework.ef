﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с уроками
    /// </summary>
    public interface IPromoCodeRepository : IRepository<PromoCode, int>
    {
        Task<List<PromoCode>> GetPagedAsync(int page, int itemsPerPage);
    }
}
