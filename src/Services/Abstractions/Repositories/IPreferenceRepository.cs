﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с уроками
    /// </summary>
    public interface IPreferenceRepository: IRepository<Preference, int>
    {
        // Task<List<Preference>> GetAsyncAll();
        Task<List<Preference>> GetPagedAsync(int page, int itemsPerPage);
    }
}
