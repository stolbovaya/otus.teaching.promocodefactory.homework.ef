﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;


namespace Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с покупателями
    /// </summary>
    public interface ICustomerRepository : IRepository<Customer, int>
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
        Task<List<Customer>> GetPagedAsync(int page, int itemsPerPage);
        int FindCustomersWithPreferenceAsync(int PreferenceId);
    }
}
