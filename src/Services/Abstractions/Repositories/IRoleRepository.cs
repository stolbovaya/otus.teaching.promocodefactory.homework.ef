﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;


namespace Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с уроками
    /// </summary>
    public interface IRoleRepository: IRepository<Role, int>
    {
        Task<List<Role>> GetPagedAsync(RoleFilterDto filterDto);
    }
}
