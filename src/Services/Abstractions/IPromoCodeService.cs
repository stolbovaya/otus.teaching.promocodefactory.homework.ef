﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Abstractions
{
    public interface IPromoCodeService
    {
       
             Task<ICollection<PromoCodeDto>> GetPaged(int page, int pageSize);

        Task<PromoCodeDto> GetById(int id);

             Task<int> Create(PromoCodeDto PromoDto);
        Task<int> GivePromocodesToCustomersWithPreferenceAsync(PromoCodeDto PromoDto);

        Task Update(int id, PromoCodeDto PromoDto);

            Task Delete(int id);
    }
}