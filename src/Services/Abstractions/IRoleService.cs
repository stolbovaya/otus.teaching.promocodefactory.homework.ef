﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Abstractions
{
    public interface IRoleService
    {

        Task<ICollection<EmployeeDto>> GetPaged(RoleFilterDto filterDto);

        Task<EmployeeDto> GetById(int id);

        Task<int> Create(RoleDto RoleDto);

        Task Update(int id, RoleDto RoleDto);

        Task Delete(int id);
    }
}