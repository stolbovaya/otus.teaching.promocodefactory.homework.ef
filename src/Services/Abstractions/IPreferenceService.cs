﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Abstractions
{
    public interface IPreferenceService
    {

        Task<ICollection<PreferencesResponseDto>> GetPaged(int page, int pageSize);
        Task<PreferenceDto> GetById(int id);
        Task<int> Create(PreferenceDto PreferenceDto);
        Task Update(int id, PreferenceDto PreferenceDto);

        Task Delete(int id);

     
    }
}