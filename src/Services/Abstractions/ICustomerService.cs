﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Abstractions
{
    public interface ICustomerService
    {

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
       
        Task<ICollection<CustomerResponseDto>> GetPaged(int page, int pageSize);

        Task<CustomerResponseDto> AddPreference(int id, int preferenceId);


        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО курса</returns>
        Task<CustomerDto> GetById(int id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="customerDto">ДТО курса</para
        Task<int> Create(CustomerDto customerDto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="customerDto">ДТО курса</param>
        Task Update(int id, CustomerDto customerDto);

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        Task Delete(int id);
    }
}