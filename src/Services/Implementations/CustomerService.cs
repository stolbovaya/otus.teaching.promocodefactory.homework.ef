﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    /// <summary>
    /// Cервис работы с покупателями
    /// </summary>
    public class CustomerService : ICustomerService
    {
        private readonly IMapper _mapper;
        private readonly ICustomerRepository _customerRepository;
        private readonly IPreferenceRepository _preferenceRepository;
        //      private readonly IBusControl _busControl;

        public CustomerService(
            IMapper mapper,
            ICustomerRepository customerRepository,
            IPreferenceRepository preferenceRepository)
        //  IBusControl busControl)
        {
            _mapper = mapper;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
          //  _busControl = busControl;
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param page, pageSize> ДТО фильтра. </param>
        /// <returns> Список покупателей. </returns>
        public async Task<ICollection<CustomerResponseDto>> GetPaged(int page, int pageSize)
        {
            ICollection<Customer> entities = await _customerRepository.GetPagedAsync (page, pageSize);
            return _mapper.Map<ICollection<Customer>, ICollection<CustomerResponseDto>>(entities);
        }
        public async Task<CustomerResponseDto> AddPreference(int id, int preferenceId)
        {
            var preference = await _preferenceRepository.GetAsync(preferenceId);
            var customer = await _customerRepository.GetAsync(id);
            customer.Preferences.Add  (preference);
            _customerRepository.Update(customer);
            await _customerRepository.SaveChangesAsync();

            return _mapper.Map<Customer, CustomerResponseDto>(customer);
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО покупателя</returns>
        public async Task<CustomerDto> GetById(int id)
        {
            var customer = await _customerRepository.GetAsync(id);
            return _mapper.Map<Customer, CustomerDto>(customer);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="CustomerDto">ДТО покупателя</param>
        /// <returns>идентификатор</returns>
        public async Task<int> Create(CustomerDto customerDto)
        {
            var entity = _mapper.Map<CustomerDto, Customer>(customerDto);
            var res = await _customerRepository.AddAsync(entity);
            await _customerRepository.SaveChangesAsync();
         return res.Id;
        }
   
        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="CustomerDto">ДТО покупателя</param>
        public async Task Update(int id, CustomerDto customerDto)
        {
            var entity = _mapper.Map<CustomerDto, Customer>(customerDto);
            entity.Id = id;
            _customerRepository.Update(entity);
            await _customerRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        public async Task Delete(int id)
        {
            _customerRepository.Delete(id);
            await _customerRepository.SaveChangesAsync();
        }
    }
}