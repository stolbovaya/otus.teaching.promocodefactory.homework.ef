﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    /// <summary>
    /// Cервис работы с курсами
    /// </summary>
    public class EmployeeService : IEmployeeService
    {
        private readonly IMapper _mapper;
        private readonly IEmployeeRepository _employeeRepository;
        //      private readonly IBusControl _busControl;

        public EmployeeService(
            IMapper mapper,
            IEmployeeRepository employeeRepository)
          //  IBusControl busControl)
        {
            _mapper = mapper;
            _employeeRepository = employeeRepository;
          //  _busControl = busControl;
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
        public async Task<ICollection<EmployeeDto>> GetPaged(EmployeeFilterDto filterDto)
        {
            ICollection<Employee> entities = await _employeeRepository.GetPagedAsync(filterDto);
            return _mapper.Map<ICollection<Employee>, ICollection<EmployeeDto>>(entities);
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО курса</returns>
        public async Task<EmployeeShortDto> GetById(int id)
        {
            var employee = await _employeeRepository.GetAsync(id);
            return _mapper.Map<Employee, EmployeeShortDto>(employee);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="EmployeeDto">ДТО курса</param>
        /// <returns>идентификатор</returns>
        public async Task<int> Create(EmployeeDto employeeDto)
        {
            var entity = _mapper.Map<EmployeeDto, Employee>(employeeDto);
            var res = await _employeeRepository.AddAsync(entity);
            await _employeeRepository.SaveChangesAsync();
            //await _busControl.Publish(new MessageDto
            //{
            //    Content = $"Lesson {entity.Id} with name {entity.Name} is added"
            //});
            return res.Id;
        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="EmployeeDto">ДТО курса</param>
        public async Task Update(int id, EmployeeDto employeeDto)
        {
            var entity = _mapper.Map<EmployeeDto, Employee>(employeeDto);
            entity.Id = id;
            _employeeRepository.Update(entity);
            await _employeeRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        public async Task Delete(int id)
        {
            var employee = await _employeeRepository.GetAsync(id);
           // employee.Delete(id) = true;
            await _employeeRepository.SaveChangesAsync();
        }
    }
}