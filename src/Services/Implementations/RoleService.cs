﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    /// <summary>
    /// Cервис работы с курсами
    /// </summary>
    public class RoleService : IRoleService
    {
        private readonly IMapper _mapper;
        private readonly IRoleRepository _roleRepository;
        //      private readonly IBusControl _busControl;

        public RoleService(
            IMapper mapper,
            IRoleRepository roleRepository)
          //  IBusControl busControl)
        {
            _mapper = mapper;
            _roleRepository = roleRepository;
          //  _busControl = busControl;
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
        public async Task<ICollection<RoleDto>> GetPaged(RoleFilterDto filterDto)
        {
            ICollection<Role> entities = await _roleRepository.GetPagedAsync(filterDto);
            return _mapper.Map<ICollection<Role>, ICollection<RoleDto>>(entities);
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО курса</returns>
        public async Task<RoleDto> GetById(int id)
        {
            var role = await _roleRepository.GetAsync(id);
            return _mapper.Map<Role, RoleDto>(role);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="RoleDto">ДТО курса</param>
        /// <returns>идентификатор</returns>
        public async Task<int> Create(RoleDto roleDto)
        {
            var entity = _mapper.Map<RoleDto, Role>(roleDto);
            var res = await _roleRepository.AddAsync(entity);
            await _roleRepository.SaveChangesAsync();
            //await _busControl.Publish(new MessageDto
            //{
            //    Content = $"Lesson {entity.Id} with name {entity.Name} is added"
            //});
            return res.Id;
        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="RoleDto">ДТО курса</param>
        public async Task Update(int id, RoleDto roleDto)
        {
            var entity = _mapper.Map<RoleDto, Role>(roleDto);
            entity.Id = id;
            _roleRepository.Update(entity);
            await _roleRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        public async Task Delete(int id)
        {
            var role = await _roleRepository.GetAsync(id);
           // role.Delete(id) = true;
            await _roleRepository.SaveChangesAsync();
        }

        Task<ICollection<EmployeeDto>> IRoleService.GetPaged(RoleFilterDto filterDto)
        {
            throw new NotImplementedException();
        }

        Task<EmployeeDto> IRoleService.GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}