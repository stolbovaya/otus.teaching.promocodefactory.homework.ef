﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    /// <summary>
    /// Cервис работы с курсами
    /// </summary>
    public class PromoCodeService : IPromoCodeService
    {
        private readonly IMapper _mapper;
        private readonly IPromoCodeRepository _promoCodeRepository;
        private readonly ICustomerRepository _customerRepository;
        //      private readonly IBusControl _busControl;

        public PromoCodeService(
            IMapper mapper,
            IPromoCodeRepository promoCodeRepository,
            ICustomerRepository customerRepository)
          //  IBusControl busControl)
        {
            _mapper = mapper;
            _promoCodeRepository = promoCodeRepository;
            _customerRepository = customerRepository;

          //  _busControl = busControl;
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
        public async Task<ICollection<PromoCodeDto>> GetPaged(int page, int pageSize)
        {
            ICollection<PromoCode> entities = await _promoCodeRepository.GetPagedAsync(page, pageSize);
            return _mapper.Map<ICollection<PromoCode>, ICollection<PromoCodeDto>>(entities);
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО курса</returns>
        public async Task<PromoCodeDto> GetById(int id)
        {
            var promoCode = await _promoCodeRepository.GetAsync(id);
            return _mapper.Map<PromoCode, PromoCodeDto>(promoCode);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="PromoCodeDto">ДТО курса</param>
        /// <returns>идентификатор</returns>
        public async Task<int> Create(PromoCodeDto promoCodeDto)
        {
  
            var entity = _mapper.Map<PromoCodeDto, PromoCode>(promoCodeDto);
            var res = await _promoCodeRepository.AddAsync(entity);
            await _promoCodeRepository.SaveChangesAsync();
            //await _busControl.Publish(new MessageDto
            //{
            //    Content = $"Lesson {entity.Id} with name {entity.Name} is added"
            //});
            return res.Id;
        }
        public async Task<int> GivePromocodesToCustomersWithPreferenceAsync(PromoCodeDto promoCodeDto)
        {

            var entity = _mapper.Map<PromoCodeDto, PromoCode>(promoCodeDto);
            var customerId =  _customerRepository.FindCustomersWithPreferenceAsync( promoCodeDto.PreferenceId) ;
            entity.CustomerId = customerId;

            var res = await _promoCodeRepository.AddAsync(entity);
            await _promoCodeRepository.SaveChangesAsync();
   
            return res.Id;
        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="PromoCodeDto">ДТО курса</param>
        public async Task Update(int id, PromoCodeDto promoCodeDto)
        {
            var entity = _mapper.Map<PromoCodeDto, PromoCode>(promoCodeDto);
            entity.Id = id;
            _promoCodeRepository.Update(entity);
            await _promoCodeRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        public async Task Delete(int id)
        {
            var promoCode = await _promoCodeRepository.GetAsync(id);
           // promoCode.Delete(id) = true;
            await _promoCodeRepository.SaveChangesAsync();
        }


    }
}