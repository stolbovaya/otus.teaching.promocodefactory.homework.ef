﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using System.Threading;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    /// <summary>
    /// Cервис работы с курсами
    /// </summary>
    public class PreferenceService : IPreferenceService
    {
        private readonly IMapper _mapper;
        private readonly IPreferenceRepository _preferenceRepository;
        //      private readonly IBusControl _busControl;

        public PreferenceService(
            IMapper mapper,
            IPreferenceRepository preferenceRepository)
          //  IBusControl busControl)
        {
            _mapper = mapper;
            _preferenceRepository = preferenceRepository;
          //  _busControl = busControl;
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
        public async Task<ICollection<PreferencesResponseDto>> GetPaged(int page, int pageSize)
        {
            ICollection<Preference> entities = await _preferenceRepository.GetPagedAsync(page, pageSize);
            return _mapper.Map<ICollection<Preference>, ICollection<PreferencesResponseDto>>(entities);
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО курса</returns>
        public async Task<PreferenceDto> GetById(int id)
        {
            var preference = await _preferenceRepository.GetAsync(id);
            return _mapper.Map<Preference, PreferenceDto>(preference);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="PreferenceDto">ДТО курса</param>
        /// <returns>идентификатор</returns>
        public async Task<int> Create(PreferenceDto preferenceDto)
        {
            var entity = _mapper.Map<PreferenceDto, Preference>(preferenceDto);
            var res = await _preferenceRepository.AddAsync(entity);
            await _preferenceRepository.SaveChangesAsync();

            return res.Id;
        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="PreferenceDto">ДТО курса</param>
        public async Task Update(int id, PreferenceDto preferenceDto)
        {
            var entity = _mapper.Map<PreferenceDto, Preference>(preferenceDto);
            entity.Id = id;
            _preferenceRepository.Update(entity);
            await _preferenceRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        public async Task Delete(int id)
        {
            var preference = await _preferenceRepository.GetAsync(id);
           // preference.Delete(id) = true;
            await _preferenceRepository.SaveChangesAsync();
        }

        //public Task<ICollection<PreferenceDto>> GetPaged(PreferenceFilterDto filterDto)
        //{
        //    //throw new NotImplementedException();

        //}
    }
}