using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;



namespace Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности курса.
    /// </summary>
    /// 


    public class CustomerMappingsProfile : Profile
    {
        public CustomerMappingsProfile()
        {
            CreateMap<Customer, CustomerDto>();

            CreateMap<CustomerDto, Customer>();

            CreateMap<Customer, CustomerResponseDto>();

           // CreateMap<Customer, CustomerShortDto>();

          //  CreateMap<CustomerShortDto, Customer>()
          //      .ForMember(d => d.PromoCodes, map => map.Ignore())
           //     .ForMember(d => d.Preferences, map => map.Ignore());
               //  .ForMember(d => d.Customer, map => map.Ignore());




        }
    }
}
