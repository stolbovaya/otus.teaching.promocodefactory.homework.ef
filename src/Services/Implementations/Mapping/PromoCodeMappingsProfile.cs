using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;



namespace Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности курса.
    /// </summary>
    /// 


    public class PromoCodeMappingsProfile : Profile
    {
        public PromoCodeMappingsProfile()
        {
            CreateMap<PromoCode, PromoCodeDto>();

            CreateMap<PromoCodeDto, PromoCode>();
                //.ForMember(d => d.PartnerName, map => map.Ignore());


            CreateMap<PromoCode, PromoCodeShortDto>();


            CreateMap<PromoCodeShortDto, PromoCode>()
                .ForMember(d => d.PartnerManager, map => map.Ignore())
                .ForMember(d => d.Preference, map => map.Ignore())
                .ForMember(d => d.Customer, map => map.Ignore());
            //    .ForMember(d => d.PartnerName, map => map.Ignore());
            // .ForMember(d => d.CustomerId, map => map.Ignore());
        }
    }
}
