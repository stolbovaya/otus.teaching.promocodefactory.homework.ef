using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;



namespace Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности курса.
    /// </summary>
    /// 


    public class PreferenceMappingsProfile : Profile
    {
        public PreferenceMappingsProfile()
        {
            CreateMap<Preference, PreferenceDto>();

            CreateMap<PreferenceDto, Preference>()
               .ForMember(d => d.PromoCodes, map => map.Ignore())
                .ForMember(d => d.Customers, map => map.Ignore());




            CreateMap<Preference, PreferencesResponseDto>();
        }
    }
}
