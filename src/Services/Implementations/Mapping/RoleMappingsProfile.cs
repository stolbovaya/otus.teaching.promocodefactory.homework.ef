using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;



namespace Otus.Teaching.PromoCodeFactory.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности курса.
    /// </summary>
    /// 


    public class RoleMappingsProfile : Profile
    {
        public RoleMappingsProfile()
        {
            CreateMap<Role, RoleDto>();

            CreateMap<RoleDto, Role>();

            CreateMap<Role, RoleShortDto>();
              //   .ForMember(d => d.Employee, map => map.Ignore());
        }
    }
}
