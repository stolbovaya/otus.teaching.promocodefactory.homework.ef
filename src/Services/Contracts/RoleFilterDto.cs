
namespace Otus.Teaching.PromoCodeFactory.Services.Contracts
{

    public class RoleFilterDto
    {
        public string Name { get; set; }

       public int ItemsPerPage { get; set; }

        public int Page { get; set; }
    }
}