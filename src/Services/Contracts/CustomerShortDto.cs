﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Services.Contracts
{
    public class CustomerShortDto
        
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }



    }
}