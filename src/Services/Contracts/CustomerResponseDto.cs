﻿namespace Otus.Teaching.PromoCodeFactory.Services.Contracts
{
    public class CustomerResponseDto

    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

         public string Email { get; set; }

        public List<PreferencesResponseDto> Preferences { get; set; }
        public List<PromoCodeShortDto> PromoCodes { get; set; }

    }
}