﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Services.Contracts
{
    public class CustomerDto
        
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes 
        public  List<PreferenceDto> Preferences { get; set; }

        public  List<PromoCodeDto> PromoCodes { get; set; }

    }
}