
namespace Otus.Teaching.PromoCodeFactory.Services.Contracts
{

    public class PromoCodeFilterDto
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public int ItemsPerPage { get; set; }

        public int Page { get; set; }
    }
}