﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Contracts
{
        /// <summary>
        /// ДТО курса
        /// </summary>
        public class EmployeeDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";
        public string Email { get; set; }
        public  RoleDto Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
    
}
