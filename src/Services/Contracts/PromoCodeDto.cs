﻿using System;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Services.Contracts
{
    public class PromoCodeDto
        
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public string ServiceInfo { get; set; }


        public string BeginDate { get; set; }

        public string EndDate { get; set; }

      
        public EmployeeDto PartnerManager { get; set; }

        public PreferenceDto Preference { get; set; }

        public  CustomerDto Customer { get; set; }

        public int PartnerManagerId { get; set; }
        public int PreferenceId { get; set; }
        public int? CustomerId { get; set; }
    }
}