
namespace Otus.Teaching.PromoCodeFactory.Services.Contracts
{

    public class PreferenceFilterDto
    {
        public string Name { get; set; }

       public int ItemsPerPage { get; set; }

        public int Page { get; set; }
    }
}