
namespace Otus.Teaching.PromoCodeFactory.Services.Contracts
{

    public class EmployeeFilterDto
    {

        public string Email { get; set; }

        public int ItemsPerPage { get; set; }

        public int Page { get; set; }
    }
}