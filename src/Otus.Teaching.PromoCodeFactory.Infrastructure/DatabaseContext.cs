﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Azure;
using Castle.Core.Resource;

namespace Otus.Teaching.PromoCodeFactory.Infrastructure
{
    /// <summary>
    /// Контекст
    /// </summary>
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        /// <summary>
        /// Работник
        /// </summary>
        public DbSet<Employee> Employee { get; set; }

        /// <summary>
        /// Роли
        /// </summary>
        public DbSet<Role> Role { get; set; }

        /// <summary>
        /// Продовец
        /// </summary>
       // public DbSet<Customer>  Customers { get; set; }

        /// <summary>
        /// Предпочтение
        /// </summary>
       // public DbSet<Preference> Preferences { get; set; }


        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }

        /// <summary>
        /// Промокод
        /// </summary>
        public DbSet<PromoCode> PromoCodes { get; set; }

       // public DbSet<CustomerPreference> CustomerPreferences { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Employee>()
                        .HasOne(r => r.Role)
                        .WithMany(e => e.Employee)
                        .HasForeignKey(r=> r.RoleId);



            modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.Email).HasMaxLength(200);

            modelBuilder.Entity<Role>().Property(c => c.Name).HasMaxLength(100);
            modelBuilder.Entity<Role>().Property(c => c.Description).HasMaxLength(500);


            modelBuilder.Entity<PromoCode>()
                .HasOne(e => e.Preference)
                .WithMany(e => e.PromoCodes);



            modelBuilder.Entity<PromoCode>().Property(c => c.Code).HasMaxLength(10);
            modelBuilder.Entity<PromoCode>().Property(c => c.ServiceInfo).HasMaxLength(10);

            modelBuilder.Entity<Preference>().Property(c => c.Name).HasMaxLength(10);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Preferences)
                .WithMany(e => e.Customers);
             //   .UsingEntity("CustomerPreference";



            modelBuilder.Entity<Customer>()
               .HasMany(e => e.PromoCodes)
               .WithOne(e => e.Customer);

            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(200);


        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
           // optionsBuilder.UseSqlite($"DataSource = ManyToManyWithNavsToJoinClass{GetType().Name}.db");
        }

    }
    
       
}


