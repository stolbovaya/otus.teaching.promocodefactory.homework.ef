﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Infrastructure;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Infrastructure.Repositories.Implementations;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Azure;

namespace Infrastructure.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с курсами
    /// </summary>
    public class PreferenceRepository : EfRepository<Preference, int>, IPreferenceRepository
    {
        public PreferenceRepository(DatabaseContext context): base(context)
        {
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="page,itemsPerPage"> номер страницы и количество элементов на странице. </param>
        /// <returns>  </returns>
        public async Task<List<Preference>> GetPagedAsync(int page, int itemsPerPage)
        {

            var query = GetAll();
 
            return await query
                .Skip((page - 1) * itemsPerPage)
                .Take(itemsPerPage)
                .ToListAsync();

        }

        /// <summary>
        /// Удаление .
        /// </summary>
        /// <param name="id"> . </param>
        /// <returns>  </returns>
        public bool Delete(int Id)
        {
             Context.Set<Preference>().Where(x => x.Id == Id).ExecuteDelete();

            return true;
        }

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        public override Task<Preference> GetAsync(int id)
        {
            var query = Context.Set<Preference>().AsQueryable();
            query = query
                  .Where(c => c.Id == id);

            return query.SingleOrDefaultAsync();
        }
    }
}
