﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Infrastructure;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Infrastructure.Repositories.Implementations;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с курсами
    /// </summary>
    public class PromoCodeRepository: EfRepository<PromoCode, int>, IPromoCodeRepository
    {
        public PromoCodeRepository(DatabaseContext context): base(context)
        {
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
        public async Task<List<PromoCode>> GetPagedAsync(int page, int itemsPerPage)
        {

            var query = GetAll();

            return await query
                .Include(c => c.PartnerManager)
                .Include(c => c.Preference)
                .Skip((page - 1) * itemsPerPage)
                .Take(itemsPerPage)
                .ToListAsync();

        }

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        public override Task<PromoCode> GetAsync(int id)
        {
            var query = Context.Set<PromoCode>().AsQueryable();
            query = query
                .Include(c => c.PartnerManager)
                .Include(c => c.Preference)
                .Include(c => c.Customer)
                .Where(c => c.Id == id);

            return query.SingleOrDefaultAsync();
        }
    }
}
