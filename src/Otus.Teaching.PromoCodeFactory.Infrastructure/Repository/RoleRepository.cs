﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Infrastructure;
using Otus.Teaching.PromoCodeFactory.Infrastructure.Repositories.Implementations;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Infrastructure.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с курсами
    /// </summary>
    public class RoleRepository: EfRepository<Role, int>, IRoleRepository
    {
        public RoleRepository(DatabaseContext context): base(context)
        {
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
        public async Task<List<Role>> GetPagedAsync(RoleFilterDto filterDto)
        {

            var query =  GetAll().ToList().AsQueryable();

            if (!string.IsNullOrWhiteSpace(filterDto.Name))
            {
                query = query.Where(c => c.Name == filterDto.Name);
            }

            //if (!string.IsNullOrWhiteSpace(filterDto.FulltName))
            //{
            //    query = query.Where(c => c.FulltName == filterDto.FulltName);
            //}

            query = query
                .Skip((filterDto.Page - 1) * filterDto.ItemsPerPage)
                .Take(filterDto.ItemsPerPage);

            
            return await query.ToListAsync();
        }

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        public override Task<Role> GetAsync(int id)
        {
            var query = Context.Set<Role>().AsQueryable();
            query = query
                //.Include(c => c.Lessons)
                .Where(c => c.Id == id);

            return query.SingleOrDefaultAsync();
        }
    }
}
