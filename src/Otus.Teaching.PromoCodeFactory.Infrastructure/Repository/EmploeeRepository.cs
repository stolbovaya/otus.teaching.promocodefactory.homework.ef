﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Infrastructure;
using Otus.Teaching.PromoCodeFactory.Infrastructure.Repositories.Implementations;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с курсами
    /// </summary>
    public class EmployeeRepository: EfRepository<Employee, int>, IEmployeeRepository
    {
        public EmployeeRepository(DatabaseContext context): base(context)
        {
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
        public async Task<List<Employee>> GetPagedAsync(EmployeeFilterDto filterDto)
        {

            var query =  GetAll().ToList().AsQueryable();

            if (!string.IsNullOrWhiteSpace(filterDto.Email))
            {
                query = query.Where(c => c.Email == filterDto.Email);
            }

            //if (!string.IsNullOrWhiteSpace(filterDto.FulltName))
            //{
            //    query = query.Where(c => c.FulltName == filterDto.FulltName);
            //}

            query = query
                .Skip((filterDto.Page - 1) * filterDto.ItemsPerPage)
                .Take(filterDto.ItemsPerPage);

            
            return await query.ToListAsync();
        }

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        public override Task<Employee> GetAsync(int id)
        {
            var query = Context.Set<Employee>().AsQueryable();
            query = query
                .Include(c => c.Role)
                .Where(c => c.Id == id);

            return query.SingleOrDefaultAsync();
        }
    }
}
