﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Infrastructure;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Infrastructure.Repositories.Implementations;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Services.Repositories.Abstractions;
using Azure;
using System.Reflection;

namespace Infrastructure.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с курсами
    /// </summary>
    public class CustomerRepository: EfRepository<Customer, int>, ICustomerRepository
    {
        public CustomerRepository(DatabaseContext context): base(context)
        {
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список курсов. </returns>
        public async Task<List<Customer>> GetPagedAsync(int page, int itemsPerPage)
        {

            var query = GetAll();
 
            return await query
                .Include(o => o.Preferences)
                .Include(c => c.PromoCodes)
                .Skip((page - 1) * itemsPerPage)
                .Take(itemsPerPage)
                .ToListAsync();

        }
        public  bool Delete(int Id)
        {
             Context.Set<Customer>().Where(x => x.Id == Id).ExecuteDelete();

            return true;
        }

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        public override Task<Customer> GetAsync(int id)
        {
            var query = Context.Set<Customer>().AsQueryable();
            query = query
                .Include(c => c.Preferences)
                .Include(c => c.PromoCodes)
                .Where(c => c.Id == id);

            return query.SingleOrDefaultAsync();
        }

        public  int FindCustomersWithPreferenceAsync(int PreferenceId)
        {
            var query = Context.Set<Preference>().AsQueryable();
            var customerId = query
              .Where(x => x.Id == PreferenceId)
              .SelectMany(x => x.Customers.Select(u => u.Id)).First();
   
            return customerId;
        }
    }
}
