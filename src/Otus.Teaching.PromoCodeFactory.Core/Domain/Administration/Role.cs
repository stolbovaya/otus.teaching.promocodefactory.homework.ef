﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
      : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public  virtual List<Employee> Employee { get; set; }
    }
}