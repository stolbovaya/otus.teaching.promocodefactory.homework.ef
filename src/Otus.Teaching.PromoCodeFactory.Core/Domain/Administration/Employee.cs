﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee : IEntity<int>  
         {
           

            public int Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }

            public string FullName => $"{FirstName} {LastName}";

            public string Email { get; set; }

            public int RoleId { get; set; }
            public Role Role { get; set; }

            public int AppliedPromocodesCount { get; set; }


        public Employee(int id,int roleId, string firstName, string lastName, string email, int appliedPromocodesCount)
        { 
            Id = id;
            RoleId=roleId;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            AppliedPromocodesCount=appliedPromocodesCount;

        }
        public void UpdateAppliedPromocodesCount(int appliedPromocodesCount)
        {

            AppliedPromocodesCount = appliedPromocodesCount;
        }
        
    }
}