﻿using System;
using System.Collections.Generic;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
       : IEntity<int>
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public string ServiceInfo { get; set; }


        public string BeginDate { get; set; }

        public string EndDate { get; set; }
        public int PartnerManagerId { get; set; }
        public Employee PartnerManager { get; set; }

        public Preference Preference { get; set; }

        public int PreferenceId {get;set;}


        public virtual Customer Customer { get; set; }
        public int? CustomerId { get; set; }




    }
}