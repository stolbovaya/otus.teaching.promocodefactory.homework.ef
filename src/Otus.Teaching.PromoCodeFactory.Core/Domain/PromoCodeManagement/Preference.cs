﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public  List<PromoCode> PromoCodes { get; set; }

        public List<Customer> Customers { get; set; }
    }
}