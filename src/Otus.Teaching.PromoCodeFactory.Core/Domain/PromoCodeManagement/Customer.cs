﻿using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
 
    public class Customer
        : IEntity<int>
    {
        public int Id { get; set; }
        //<summary>
        // Name
        //</summary>
        public string FirstName { get; set; }
        //<summary>
        // Фамилия
        //</summary>
        public string LastName { get; set; }
        //<summary>
        // Имя и Фамилия
        //</summary>
        public string FullName => $"{FirstName} {LastName}";
        //<summary>
        // Почта
        //</summary>
        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes 
        public List<Preference> Preferences { get; } = new();
        
        public  List<PromoCode> PromoCodes { get; set; }


    }
}